<?php
/**
 * The Template for displaying pages
 *
 * @package WordPress
 * @subpackage 1428268365
 */
get_header();

while (have_posts()) {
  the_post();

  echo '<h2 class="subhead">';
  echo get_the_title();
  echo edit_post_link( 'edit', '<span class="edit-link"> ', '</span>' );
  echo '</h2>';

  echo get_template_part('content');

}

get_footer();
