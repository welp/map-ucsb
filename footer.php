<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage 1428268365
 */
?>

</div> <!-- .read -->
</main><!-- #main -->

<footer id="colophon" class="site-footer" role="contentinfo">

  <div class="footer-text">
  <p>Part of the national <a href="http://www.mapforthegap.com">MAP network</a></p>

<p>Minorities and Philosophy (MAP) is generously supported by the Marc
Sanders Foundation, which “aims to stimulate renewed appreciation for
traditional philosophy by encouraging, identifying and rewarding
excellent research” in philosophy. For more on the MSF, visit their <a href="http://www.marcsandersfoundation.org/">webpage</a>.</p>

  <ul>
  <li><a href="http://philosophy.ucsb.edu/life/map/" title="Contact page">Contact</a></li>
  <li><a href="/wp-admin/" title="WordPress login">Log in</a></li>
  </ul>

  <p id="art-credit">“<a href="http://americanart.si.edu/collections/search/artwork/?id=22494" title="Smithsonian American Art Museum">Gwendolyn</a>” is by John Sloan and resides in the public domain.</p>

  </div>

  <img class="logo"
  src="/wp-content/themes/<?php echo get_template_directory_uri(); ?>/images/MAP_logo_light_169x87.png"
  alt="MAP logo"
  srcset="/wp-content/themes/<?php echo get_template_directory_uri(); ?>/images/MAP_logo_light_169x87.png 1x,
  /wp-content/themes/<?php echo get_template_directory_uri(); ?>/images/MAP_logo_light_338x174.png 2x,
  /wp-content/themes/<?php echo get_template_directory_uri(); ?>/images/MAP_logo_light_508x262.png 3x">

</footer>

<!-- <picture> polyfill -->
<script src="//cdn.jsdelivr.net/picturefill/2.2.0/picturefill.min.js" async defer></script>
</body>
</html>
