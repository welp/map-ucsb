<?php
/**
 * The template for displaying the events archive
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage 1428268365
 */
get_header();

echo '<h2 class="subhead">Events archive</h2>';

$catid = get_cat_ID('event');
$events = get_posts(array('category' => $catid));
$events_newest = array_reverse(sort_events_oldest_first($events));
foreach ($events_newest as $i => $event) {
  echo '<h3 class="entry-title">';
  $now = (event_has_started($event->ID) && !event_has_ended($event->ID));
  $event_time = simple_fields_value('startdate',get_the_ID())["ISO_8601"];
  echo '<time datetime="' . $event_time . '" class="title event ';
  if ($now) {
    echo 'happening">Happening now:</time> ';
  }
  else {
    if (!event_has_started(get_the_ID())) {
      echo '">';
    }
    else {
      echo 'past">';
    }
    echo simple_fields_value('startdate',get_the_ID())["date_format"];
    echo '</time> ';
  }
  echo '<a href="' . get_permalink($event->ID);
  echo '" title="Permanent link to event page">';
  echo $event->post_title;
  echo '</a>';
  echo ' (in ' . simple_fields_value('location',$event->ID) . ')';
  echo edit_post_link( 'edit', '<span class="edit-link"> ', '</span>', $event->ID );
  echo '</h3>';
  echo '<div class="sub">';
  // http://stackoverflow.com/a/5228367
  echo apply_filters('the_content', $event->post_content);
  echo '</div>';
}

if ( !have_posts() ) {
  echo '<h3>No events found!</h3>';
  echo '<p>Sorry ¯\_(ツ)_/¯</p>';
}

get_footer();
