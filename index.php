<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 **/
get_header();

if ( have_posts() ) {
  if ( is_home() && !is_front_page() ) {
    echo '<h1 class="page-title screen-reader-text">';
    single_post_title();
    echo '</h1>';
  }

  $index_post_counter = 0;
  while ( have_posts() && $index_post_counter < 6) {
    the_post();
    $cats = get_the_category()[0]->name;
    if ($cats === 'discussion') {
      get_template_part( 'content' );
      $index_post_counter += 1;
    }
  }
}
echo '<hr class="major">';
$archive_url = get_category_link(get_cat_ID('discussion'));
echo '<div class="more">';
echo '<p class="index-footer"><a title="Discussions archive" href="';
echo $archive_url . '">See all posts</a></p>';
echo '</div>';

get_footer();
