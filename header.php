<?php
/**
 * The Header for our theme
 *
 * @package WordPress
 * @subpackage 1428268365
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- viewport settings for small-screen devices; see:
      http://www.quirksmode.org/blog/archives/2010/09/combining_meta.html -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!--[if lt IE 9]>
<!-- shim to force old IE to recognize HTML5 elements -->
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<?php
echo '<title>';
echo wp_title( '|', true, 'right' );
echo get_bloginfo('name');
echo '</title>';

echo '<meta name="description" content="';
if ( is_single() ) {
  the_excerpt();
}
else {
  echo 'Minorities and Philosophy aims to examine and address issues of minority participation in academic philosophy.';
}
echo '">';
?>

<link href="/wp-content/themes/<?php echo get_template_directory_uri(); ?>/css/style.css" rel="stylesheet" type="text/css" media="screen">
<link rel="alternate" type="application/rss+xml" title="RSS Feed" href="/?feed=rss2">
</head>

<body>
<a class="screen-reader-text skip-link" href="#content">Skip to content</a>
<?php
echo '<header id="masthead"';
if (is_home()) {
  echo ' class="index">';
}
else {
  echo ' class="single">';
}

if (!is_home()) {
  echo '<div class="head-left">';
  echo '<a class="go-home" href="/" title="Main page">Home</a>';
  echo '</div>'; // head-left
}

echo '<div class="head-right">';
echo '<div class="search header-search">';
get_search_form();
echo '</div>'; // search div
echo '</div>';

echo '<a href="/" title="Main page">';
echo '<h1 class="headline';
if (!is_home()) { echo ' hidden'; }
echo '">Minorities and Philosophy</h1></a>';

echo '</header>';

echo '<main>';
if (is_home() || is_page() ) {
  echo '<div class="meta';
  if (is_home() || is_page()) { echo '-index'; }
  echo '">';
  echo upcoming_events();
  echo make_menu();
  echo members_list();
  echo projects_list();
  echo '</div>';
}
echo '<div class="read';
if (is_home() || is_page()) { echo '-index'; }
echo '">';
