<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage 1428268365
 */
get_header(); ?>

<h2 class="subhead">File not found</h2>
<p>It looks like nothing was found at this location. Maybe try a search?</p>

<?php
get_footer();
