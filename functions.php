<?php
/**
 * 1428268365 functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used
 * in the theme as 1428268365 template tags. Others are attached to action
 * and filter hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those
 * wrapped in a function_exists() call) by defining them first in your
 * child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * @link http://codex.wordpress.org/Theme_Development
 * @link http://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link http://codex.wordpress.org/Plugin_API
 *
 * @package WordPress
 * @subpackage 1428268365
 */

function menu_setup() {
  register_nav_menu( 'top', 'Top bar menu' );
}
add_action( 'after_setup_theme', 'menu_setup' );

// remove the bad toolbar items
function simplify_mce($settings) {
  $settings['toolbar1'] = "bold,italic,blockquote,bullist,numlist,link,unlink,table,fullscreen,undo,redo,wp_adv,spellchecker,dfw";
  $settings['toolbar2'] = "formatselect,strikethrough,pastetext,removeformat,charmap,wp_more,wp_help";
  return $settings;
}
add_filter( 'tiny_mce_before_init', 'simplify_mce' );

function make_menu() {
  $menu_opts = [
    'container'       => 'nav',
    'menu_class'      => 'nav',
    'echo'            => 0,
    'before'          => '',
    'after'           => '',
    'link_before'     => '',
    'link_after'      => '',
    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
    'depth'           => 1
  ];
  return wp_nav_menu($menu_opts);
}

function active_member_field($user) {
  $user_active = get_the_author_meta( 'active-member', $user->ID);
  // no <form>; it's part of the full user form
  echo '<label for="active">Active MAP member?</label> ';
  echo '<input type="checkbox" name="active-member" id="active-member" ';
  if ($user_active) {
      echo 'checked="true"';
  }
  echo '>';
}
function save_member_status($user_id) {
  if ( !current_user_can( 'edit_user', $user_id ) ) {
    return false;
  }
  // https://wordpress.org/support/topic/how-to-add-checkbox-in-profile-page
  update_user_meta( $user_id, 'active-member', $_POST['active-member']);
}
// http://codex.wordpress.org/Plugin_API/Action_Reference/show_user_profile
add_action( 'edit_user_profile', 'active_member_field' );
add_action( 'edit_user_profile_update', 'save_member_status' );
// these make the checkbox appear higher up but don't work when looking at other user profiles:
// add_action( 'personal_options', 'active_member_field' );
// add_action( 'personal_options_update', 'save_member_status' );

function event_has_ended($post) {
// http://simple-fields.com/documentation/api/getting-values/simple_fields_value/
  // workaround for the fact that Simple Fields' Date&Time picker is
  // broken
  $end_date = simple_fields_value('enddate',$post)["date_unixtime"];
  $end_time = simple_fields_value('endtime',$post)["ISO_8601"];
  $end_unix = strtotime($end_time, $end_date);
  // http://stackoverflow.com/a/2515070
  $now_unix = strtotime('now');

  return $now_unix > $end_unix;
}

function event_has_started($post){
  $start_date = simple_fields_value('startdate',$post)["date_unixtime"];
  $start_time = simple_fields_value('starttime',$post)["ISO_8601"];
  $start_unix = strtotime($start_time, $start_date);
  $now_unix = strtotime('now');

  return $now_unix > $start_unix;
}

function event_start_unix($post){
  $start_date = simple_fields_value('startdate',$post)["date_unixtime"];
  $start_time = simple_fields_value('starttime',$post)["ISO_8601"];
  $start_unix = strtotime($start_time, $start_date);

  return $start_unix;
}

function event_end_unix($post){
  $end_date = simple_fields_value('enddate',$post)["date_unixtime"];
  $end_time = simple_fields_value('endtime',$post)["ISO_8601"];
  $end_unix = strtotime($end_time, $end_date);

  return $end_unix;
}

// sort function
// http://php.net/manual/en/function.usort.php
function by_unix($a, $b)
{
  if ($a->x_unix_start === $b->x_unix_start) {
    return 0;
  }
  return ($a->x_unix_start < $b->x_unix_start) ? -1 : 1;
}

function sort_events_oldest_first($array){
  // copy the array since usort operates directly on the array passed
  // to it
  $events = $array;
  foreach ($events as $i => $val){
    // add a new index with the unix start time
    $val->x_unix_start = event_start_unix($val->ID);
  }
  // sort by unix start time; see by_unix() sort func above
  usort($events, "by_unix");
  return $events;
}

function are_upcoming_events() {
  $category_id = get_cat_ID('event');
  $event_list = get_posts(array('category' => $category_id));
  $sorted_events = sort_events_oldest_first($event_list);

  $event_count = 0;
  foreach ($sorted_events as $item) {
    if (!event_has_ended($item->ID)) {
      $event_count += 1;
    }
  }
  return $event_count > 0;
}

function members_list() {
  $string = '<div class="membership">';
  $string .= '<p class="members">UCSB Membership</p>';
  $string .= '<ul class="members">';
  // TODO: sort by last_name
  $current_members = get_users(['meta_key' => 'active-member',
                                'meta_value' => 'on']);
  foreach ($current_members as $user) {
    $string .= '<li class="member">';
    $string .= '<a href="/author/' . $user->user_login . '" ';
    $string .= 'title="Author archive">';
    $string .= $user->display_name . '</a></li>';
  }
  $string .= '</ul></div>';
  return $string;
}

function projects_list() {
  $string = '<div class="projects">';
  $string .= '<p class="projects">Current Projects</p>';
  $string .= '<ul class="projects">';
  $current_projects = get_posts(['post_type' => 'page']);
  foreach ($current_projects as $proj) {
    // 34 is the about page
    if ($proj->ID != 34) {
      $string .= '<li class="project">';
      $string .= '<a href="' . get_permalink($proj->ID) . '" ';
      $string .= 'title="Project page">';
      $string .= $proj->post_title . '</a></li>';
    }
  }
  $string .= '</ul></div>';
  return $string;
}

function upcoming_events() {
  $e_id = get_cat_ID('event');
  if (are_upcoming_events()) {
    $events = get_posts(array('category' => $e_id));
    $sorted_events = sort_events_oldest_first($events);
    $output = '<ul class="events list infobox">';
    foreach ($sorted_events as $i => $event) {
      if (!event_has_ended($event->ID)) {
        $output .= '<li class="event">';
        $now = (event_has_started($event->ID) && !event_has_ended($event->ID));
        if ($now) {
          $output .= '<span class="event happening">Happening now:</span> ';
        }
        else {
          $output .= '<span class="event">';
          $output .= simple_fields_value('startdate',$event->ID)["date_format"];
          $output .= ':</span> ';
        }
        $output .= '<a href="' . get_permalink($event->ID);
        $output .= '" title="Permanent link to event page">';
        $output .= $event->post_title;
        $output .= '</a>';
        $output .= ' (in ' . simple_fields_value('location',$event->ID) . ')';
        $output .= '</li>';
      }
    }
    $output .= '</ul>';
  }
  return $output;
}

function is_discussed($eventID) {
  $d_id = get_cat_ID('discussion');
  $discussions = get_posts(['category' => $d_id]);
  foreach ($discussions as $p) {
    $related_event_id = simple_fields_value('event_post', $p->ID);
    if ($eventID == $related_event_id) {
      return $p->ID;
    }
  }
  return false;
}
