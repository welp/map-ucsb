<?php
/**
 * The template for displaying Category pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage 1428268365
 */
get_header();

if ( have_posts() ) {
  echo '<h2 class="subhead">';
  printf( __( '%s archive' ), single_cat_title( '', false ) );
  echo '</h2>';
}
else {
  echo '<h2 class="subhead">No posts found</h2>';
}

while ( have_posts() ) : the_post();

get_template_part( 'content' );

endwhile;

if ( !have_posts() ) {
  echo '<h3>No results found!</h3>';
  echo '<p>Sorry ¯\_(ツ)_/¯</p>';
}

get_footer();
