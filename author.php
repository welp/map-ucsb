<?php
/**
 * The template for displaying Author archive pages
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage 1428268365
 */

get_header();
echo '<h2 class="subhead">';
if ( have_posts() ) {
  /*
   * Queue the first post, that way we know what author
   * we're dealing with (if that is the case).
   *
   * We reset this later so we can run the loop properly
   * with a call to rewind_posts().
   */
  the_post();
  echo 'All posts by ';
  if (get_the_author_link()) {
    echo get_the_author_link();
  }
  else {
    echo get_the_author();
  }
}
else {
  echo 'No posts for this author';
}

echo '</h2>';

if (get_the_author_meta( 'description' )) {
  echo '<p class="author-description">';
  the_author_meta( 'description' );
  echo '</p>';
}
/*
 * Since we called the_post() above, we need to rewind
 * the loop back to the beginning that way we can run
 * the loop properly, in full.
 */
rewind_posts();

// Start the Loop.
while ( have_posts() ) {
  the_post();
  $cats = get_the_category()[0]->name;
  if ($cats === 'discussion') {
    get_template_part( 'content' );
  }
}

if (!have_posts()) {
  echo '<h2>No results found!</h2>';
  echo '<p>Sorry ¯\_(ツ)_/¯</p>';
}

get_footer();
