<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage 1428268365
 */
get_header();

if ( have_posts() ) {
  echo '<h2 class="subhead">';
  echo 'Search results for: “' . get_search_query() . "”";
  echo '</h2>';
}
else {
  echo '<h2 class="subhead">No results found for ';
  echo get_search_query();
  echo '</h2>';
}

// Start the Loop.
while (have_posts()) {
  the_post();
  get_template_part( 'content' );
}

if (!have_posts()) {
  echo '<p>Sorry ¯\_(ツ)_/¯</p>';
}

get_footer();
