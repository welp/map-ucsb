<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage 1428268365
 */

$h = is_home() ? "h2" : $h = "h3";
if ( !is_single() && !is_page() ) {
  echo '<' . $h . ' class="entry-title">';
  // 'c' gives ISO 8601
  echo '<time datetime="' . get_the_date('c') . '" class="title date">';
  echo get_the_date() . '</time> ';
  echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">';
  echo get_the_title();
  echo '</a>';
  echo edit_post_link( 'edit', '<span class="edit-link"> ', '</span>' );
  echo '</' . $h . '>';
}

// used in both conditionals below
$cats = get_the_category()[0]->name;
if ($cats === 'discussion') {
  $id = simple_fields_value('event_post');
  if ($id) {
    $event_url = get_permalink($id);
    $event_date = simple_fields_value('startdate',$id)["date_format"];
    echo '<p class="related">This is a discussion by ';
    echo the_author_posts_link();
    echo ' about “';
    echo '<a href="' . $event_url . '" title="Event page">';
    echo get_the_title($id) . '</a>';
    echo ',” which ';

    $project_id = simple_fields_value('project_page');
    if ($project_id) {
      $the_page = get_post($project_id);
    }
    if ($project_id) {
      echo 'is part of the ';
      echo '<a title="Project page" href="';
      echo get_page_link($project_id) . '">';
      echo $the_page->post_title . '</a>';
      echo ' project and ';
    }
    echo 'took place on ' . $event_date . '.';
    echo '</p>';
    echo '<hr class="minor">';
  }
}

if ($cats === 'event') {
  $discuss_id = is_discussed(get_the_ID());

  echo '<p class="related">This event';
  $project_id = simple_fields_value('event_proj');
  if ($project_id) {
    $the_page = get_post($project_id);
  }
  if ($project_id) {
    if ($discuss_id) {
      echo ', part of the ';
    }
    else {
      echo ' is part of the ';
    }
    echo '<a title="Project page" href="';
    echo get_page_link($project_id) . '">';
    echo $the_page->post_title . '</a>';
    if ($discuss_id) {
      echo ' project,';
    }
    else {
      echo ' project.';
    }
  }
  if ($discuss_id) {
    $discuss_url = get_permalink($discuss_id);
    echo ' is discussed in “';
    echo '<a href="' . $discuss_url . '" title="Permanent link to post">';
    echo get_the_title($discuss_id) . '</a>.”';
    echo '</p>';
    echo '<hr class="minor">';
  }
}

if ( !is_single() && !is_page() && !is_home() ) {
  the_excerpt();
}
else {
  the_content();
}
