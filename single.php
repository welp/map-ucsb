<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage 1428268365
 */
get_header();

while (have_posts()) {
  the_post();

  echo '<h2 class="subhead">';
  $cats = get_the_category()[0]->name;
  if ($cats === 'event') {
    $now = (event_has_started(get_the_ID()) && !event_has_ended(get_the_ID()));
    $event_time = simple_fields_value('startdate',get_the_ID())["ISO_8601"];
    echo '<time datetime="' . $event_time . '" class="title event ';
    if ($now) {
      echo 'happening">Happening now:</time> ';
    }
    else {
      if (!event_has_started(get_the_ID())) {
        echo '">';
      }
      else {
        echo 'past">';
      }
      echo simple_fields_value('startdate',get_the_ID())["date_format"];
      echo '</time> ';
    }
  }
  else { // if not event
    echo '<time datetime="' . get_the_date('c') . '" class="title date">';
    echo get_the_date() . '</time> ';
  }
  echo get_the_title();
  if ($cats === 'event') {
    echo ' (in ' . simple_fields_value('location',get_the_ID()) . ')';
  }
  echo edit_post_link( 'edit', '<span class="edit-link"> ', '</span>' );
  echo '</h2>';

  echo get_template_part('content');

  echo '<hr class="minor">';

  $catid = get_cat_ID($cats);
  $events_archive = get_category_link($catid);
  if ($cats === 'event') {
    echo '<p class="sub-footer">';
    echo '<a href="' . $events_archive . '" ';
    echo 'title="Events archive">See all upcoming and past events';
    echo '</a></p>';
  }
  else {
    echo '<p class="sub-footer">Published on ';
    echo '<time datetime="' . get_the_date('c') . '" class="post date">';
    echo get_the_date() . '</time>. ';
    echo '<a href="' . $events_archive . '" ';
    echo 'title="Discussion archive">See all posts';
    echo '</a></p>';
  }
}
get_footer();
